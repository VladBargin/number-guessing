import time
import random
from math import *

# def query(a, b):
#     return sum(map(lambda x, y: x == y, a, b))

def query(a, b):
    ans = 0
    for i in range(len(a)):
        if a[i] == b[i]:
            ans += 1
    return ans


def possible_states(query_num, number_space, n):
    matching_nums = [[] for _ in range(n + 1)]
    for number in number_space:
        qr = query(query_num, number)
        matching_nums[qr].append(number)
    #print(query_num, number_space, n)
    #print(matching_nums)
    #print()
    return matching_nums


def get_max_split(query_num, number_space, n, current_best_split):
    split_sizes = [0] * (n + 1)
    max_split = 0
    for number in number_space:
        qr = query(query_num, number)
        x = split_sizes[qr] + 1
        split_sizes[qr] = x
        if max_split < x:
            max_split = x
            if max_split >= current_best_split:
                return max_split
    return max_split


def get_start_state_space(n, k):
    if n == 1:
        return [[i] for i in range(k)]
    else:
        res = []
        prev = get_start_state_space(n - 1, k)
        for num in prev:
            for i in range(k):
                if i not in num:
                    res.append(num + [i])
        return res


def get_start_state_space1(n, k):
    if n == 1:
        return [[i] for i in range(k)]
    else:
        res = []
        prev = get_start_state_space1(n - 1, k)
        for num in prev:
            for i in range(k):
                res.append(num + [i])
        return res


def dfsf(cur_num, cur_depth, n, k, state_space, sp, qsp):
    try:
        state_space.remove(cur_num)
    except:
        pass
    statebr = possible_states(cur_num, state_space, n)
    maxd = cur_depth
    for pos_ans in range(n):
        cur_state = statebr[pos_ans]
        if len(cur_state) <= 1:
            continue
        if len(qsp) <= 0:
            best_num = cur_state[0]
            best_split = get_max_split(best_num, cur_state, n, len(cur_state) + 1)
            for number in sp:
                cur_max = get_max_split(number, cur_state, n, best_split)
                if cur_max < best_split:
                    best_split = cur_max
                    best_num = number
        else:
            best_num = qsp[-1][:]

        maxd = max(dfsf(best_num, cur_depth + 1, n, k, cur_state[:], sp, qsp[:(len(qsp) - 1)]), maxd)
        if cur_depth == 1:
            print(pos_ans)
            print()
            print()
    return maxd


n, k = map(int, input('Input n, k: ').split())

st_sp = get_start_state_space(n, k)
print('st_sp done')

qsp = []
for i in range(1, k):
    qsp.append([i] * n)
print(qsp)
ts = time.time()
maxd = dfsf([0] * n, 1, n, k, st_sp[:], st_sp, qsp[:])

te = time.time()
print(te - ts)
print(maxd)