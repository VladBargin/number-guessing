from anytree import Node, RenderTree


def query(a, b):
    ans = 0
    for i in range(len(a)):
        if a[i] == b[i]:
            ans += 1
    return ans


def num_possible_states(query_num, query_res, number_space):
    ans = 0
    for number in number_space:
        if query(query_num, number) == query_res:
            ans += 1
    return ans


def possible_states(query_num, number_space, n):
    matching_nums = [[] for _ in range(n + 1)]
    for number in number_space:
        qr = query(query_num, number)
        matching_nums[qr].append(number)
    # print(query_num, query_res, number_space, matching_nums)
    return matching_nums


def get_max_split(query_num, number_space, n, current_best_split):
    split_sizes = [0] * (n + 1)
    max_split = 0
    for number in number_space:
        qr = query(query_num, number)
        x = split_sizes[qr] + 1
        split_sizes[qr] = x
        if max_split < x:
            max_split = x
            if max_split >= current_best_split:
                return max_split
    return max_split


def get_start_state_space(n, k):
    if n == 1:
        return [[i] for i in range(k)]
    else:
        res = []
        prev = get_start_state_space(n - 1, k)
        for i in range(k):
            for num in prev:
                res.append([i] + num)
        return res


maxd = [1]


def dfsf(cur_num, cur_depth, end_depth, n, k, state_space):
    if len(state_space) == 1:
        return
    if len(state_space) <= 0:
        return
    try:
        state_space.remove(cur_num)
    except:
        pass
    maxd[0] = max(maxd[0], cur_depth)
    statebr = possible_states(cur_num, state_space, n)
    for pos_ans in range(n):
        cur_state = statebr[pos_ans]
        if len(cur_state) == 0:
            continue
        best_num = cur_state[0]
        best_split = get_max_split(best_num, cur_state, n, len(cur_state) + 1)
        for number in sp:
            cur_max = get_max_split(number, cur_state, n, best_split)
            if cur_max < best_split:
                best_split = cur_max
                best_num = number

        dfsf(best_num, cur_depth + 1, end_depth, n, k, cur_state[:])
        if cur_depth == 1:
            print(pos_ans)


d = {}

def dfs(cur_num, cur_depth, end_depth, parent_node, n, k, state_space):
    if len(state_space) == 1:
        cn = Node(str(list(state_space)[0]), parent=parent_node)
        return
    if cur_depth >= end_depth or len(state_space) <= 0:
        cn = Node('X', parent=parent_node)
        return
    cur_node = Node(str(cur_num), parent=parent_node)
    try:
        state_space.remove(cur_num)
    except:
        pass
    maxd[0] = max(maxd[0], cur_depth)
    statebr = possible_states(cur_num, state_space, n)
    for pos_ans in range(n):
        cur_state = statebr[pos_ans]
        if len(cur_state) == 0:
            dfs(None, cur_depth + 1, end_depth, cur_node, n, k, cur_state[:])
            continue
        best_num = cur_state[0]
        best_split = get_max_split(best_num, cur_state, n, len(cur_state) + 1)
        for number in sp:
            # cur_max = max(map(len, possible_states(number, cur_state, n)))
            cur_max = get_max_split(number, cur_state, n, best_split)
            if cur_max < best_split:
                best_split = cur_max
                best_num = number
        # print(cur_num, best_num, pos_ans)\

        dfs(best_num, cur_depth + 1, end_depth, cur_node, n, k, cur_state[:])
        if cur_depth == 1:
            print(pos_ans)

    # cn = Node(str(cur_num), parent=parent_node)

n, k = map(int, input('Input n, k: ').split())

sp = get_start_state_space(n, k)
print(sp)
tree = Node('Tree')
dfs([0]*n, 1, n*k + 10, tree, n, k, sp)
#dfsf([0] * n, 1, n * k + 10, n, k, sp)
for pre, fill, node in RenderTree(tree):
    print("%s%s" % (pre, node.name))
print()
print('Len state_space: ', len(sp) + 1)
print("Max depth is", maxd[0])
print("Expected max depth is", n * (k - 1))
