#include <algorithm>
#include <iostream>
#include <vector>
#include <string>

using namespace std;

int query(vector<int> a, vector<int> b, int n){
    int ans = 0;
    for (unsigned i = 0; i < n; i++) {
        if (a[i] == b[i]) {
            ans++;
        }
    }
    return ans;
}

vector< vector< vector<int> > > possible_states(vector<int> query_num, vector< vector<int> > number_space, int n) {
    vector< vector< vector<int> > > matching_nums (n + 1);
    for (int i = 0; i <= n; i++) {
        vector< vector<int> > t; 
        matching_nums[i] = t;
    } 

    for (unsigned i = 0; i < number_space.size(); i++) { 
        vector<int> number = number_space[i];
        int qr = query(query_num, number, n);
        matching_nums[qr].push_back(number);
    }
    return matching_nums;
}


int get_max_split(vector<int> query_num, vector< vector<int> > number_space, int n, int current_best_split) {
    int split_sizes[n + 1];
    for (int i = 0; i <= n; i++) {
        split_sizes[i] = 0;
    }
    int max_split = 0;
    for (unsigned i = 0; i < number_space.size(); i++) { 
        vector<int>  number = number_space[i];
        int qr = query(query_num, number, n);
        int x = split_sizes[qr] + 1;
        split_sizes[qr] = x;
        if (max_split < x) {
            max_split = x;
            if (max_split >= current_best_split) return max_split;
        }
    }
    return max_split;
}


vector< vector<int> > get_start_state_space(int n, int k, int sn) {
    vector< vector<int> > res;
    if (n == 1) {
        for (int i = 0; i < k; i++) {
            vector<int> a (sn);
            for (int j = 0; j < sn; j++) a[j] = i;
            res.push_back(a);
        }
        return res;
    }
    vector< vector<int> > prev = get_start_state_space(n - 1, k, sn);
    for (unsigned i = 0; i < prev.size(); i++) {
        for (int j = 0; j < k; j++) {
            vector<int> t;
            for (int x = 0; x < sn; x++) {
                t.push_back(prev[i][x] );
            }
            t[n - 1] = j;
            res.push_back(t);            
        }
    }
    return res;
}

int dfsf(vector<int> cur_num, int cur_depth, int end_depth, int n, int k, vector< vector<int> > state_sp, vector< vector<int> > sp) {
    if (state_sp.size() <= 1 || end_depth < cur_depth) return cur_depth - 1;
    vector< vector<int> > state_space (state_sp.size());
    unsigned ci = 0;
    for (unsigned i = 0; i < state_sp.size(); i++) { 
        vector<int> number = state_sp[i];
        if (query(number, cur_num, n) < n) { 
            state_space[ci] = number;
            ci++;
        }
    }
    state_space.resize(ci);
    vector< vector< vector<int> > > statebr = possible_states(cur_num, state_space, n);
    int res = cur_depth;
    for (unsigned pos_ans = 0; pos_ans < n; pos_ans++) {
        vector< vector<int> > cur_state = statebr[pos_ans];
        if (cur_state.size() > 0) {
            vector<int>  best_num = cur_state[0];
            int best_split = get_max_split(best_num, cur_state, n, cur_state.size() + 1);
            for (vector< vector<int> >::iterator it = sp.begin(); it != sp.end(); ++it) {
                 vector<int>  number = *it;
                int cur_max = get_max_split(number, cur_state, n, best_split);
                if (cur_max < best_split) {
                    best_split = cur_max;
                    best_num = number;
                }
            }
            res = max(dfsf(best_num, cur_depth + 1, end_depth, n, k, cur_state, sp), res);
        }
        if (cur_depth == 1) cout << pos_ans << endl;
    }
    return res;
}


int main() {
    int n, k;
    cout << "Input n, k: ";
    cin >> n >> k;
    vector< vector<int> > sp = get_start_state_space(n, k, n);
    cout << "sp done" << endl;
    vector<int> a(n);
    for (unsigned i = 0; i < n; i++) {
        a[i] = 0;
    }
    int maxd = dfsf(a, 1, n * (k - 1) + 1, n, k, sp, sp);
    cout << endl;
    cout << "Len state_space: " << sp.size() << endl;
    cout <<  "Max depth is: " << maxd << endl;
    
    //print("Expected max depth is", n * (k - 1))
}
