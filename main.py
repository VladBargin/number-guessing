import time

# def query(a, b):
#     return sum(map(lambda x, y: x == y, a, b))

def query(a, b):
    ans = 0
    for i in range(len(a)):
        if a[i] == b[i]:
            ans += 1
    return ans


def possible_states(query_num, number_space, n):
    matching_nums = [[] for _ in range(n + 1)]
    for number in number_space:
        qr = query(query_num, number)
        matching_nums[qr].append(number)
    return matching_nums


def get_max_split(query_num, number_space, n, current_best_split):
    split_sizes = [0] * (n + 1)
    max_split = 0
    for number in number_space:
        qr = query(query_num, number)
        x = split_sizes[qr] + 1
        split_sizes[qr] = x
        if max_split < x:
            max_split = x
            if max_split >= current_best_split:
                return max_split
    return max_split


def get_start_state_space(n, k):
    if n == 1:
        return [[i] for i in range(k)]
    else:
        res = []
        prev = get_start_state_space(n - 1, k)
        for num in prev:
            for i in range(k):
                res.append(num + [i])
        return res


def dfsf(cur_num, cur_depth, n, k, state_space, sp):
    try:
        state_space.remove(cur_num)
    except:
        pass

    statebr = possible_states(cur_num, state_space, n)
    maxd = cur_depth
    for pos_ans in range(n):
        cur_state = statebr[pos_ans]
        if len(cur_state) <= 1:
            continue
        best_num = cur_state[0]
        best_split = get_max_split(best_num, cur_state, n, len(cur_state) + 1)
        for number in sp:
            cur_max = get_max_split(number, cur_state, n, best_split)
            if cur_max < best_split:
                best_split = cur_max
                best_num = number

        maxd = max(dfsf(best_num, cur_depth + 1, n, k, cur_state[:], sp), maxd)
    return maxd


n, k = map(int, input('Input n, k: ').split())

st_sp = get_start_state_space(n, k)
print('st_sp done')
ts = time.time()

maxd = dfsf([0] * n, 1, n, k, st_sp[:], st_sp)

te = time.time()
print(te - ts)

print()
print('Len state_space: ', len(st_sp))
print("Max depth is", maxd)
print("Expected max depth is", n * (k - 1))
